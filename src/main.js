// const name = window.prompt('Rechercher un jeu');
// const name = 'Mario Kart 8 Deluxe';
// const html = renderGameThumbnail(name);
// document.querySelector('.gameList').innerHTML = html;





function renderGameThumbnail({name,released,metacritic,background_image}){
    const html = `<a href="${background_image}">
                    <img src="${background_image}"/>
                    <footer>
                        <h3>${name}</h3>
                        <div class="infos">
                            <time datetime="${released}">${new Date(released).toLocaleDateString()}</time>
                            <span class="metacritic">${metacritic}</span>
                        </div>
                    </footer>
                </a>`;
    return html;
}

const data = [
	{
		name: 'Mario Kart 8 Deluxe',
		released: '2017-04-27',
		metacritic: 92,
		background_image: 'images/mario-kart-8-deluxe.jpg'
	},
	{
		name: 'God of War Ragnarok',
		released: '2022-11-09',
		metacritic: 94,
		background_image: 'images/god-of-war-ragnarok.jpg',
	},
	{
		name: 'The Last of Us Part 2',
		released: '2020-06-19',
		metacritic: 94,
		background_image: 'images/the-last-of-us-part-2.jpg'
	},
];

data.sort((game1, game2)=>{
    return game1.name.localeCompare(game2.name);
})

const res=data.filter(game1=>game1.metacritic>93);
for(let i=0 ; i<data.length ; i++){
    html = renderGameThumbnail(data[i]);
    document.querySelector('.gameList').innerHTML += html;
}

res.forEach(element => document.querySelector('.gameList').innerHTML += renderGameThumbnail(element));
